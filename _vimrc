set runtimepath+=~/.vim/
source ~/.vim/basic.vim
source ~/.vim/plugins.vim
source ~/.vim/keymap.vim
source ~/.vim/user.vim
