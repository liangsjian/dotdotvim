# dotvim

个人vim配置,使用Vundle进行插件管理

# 插件集合

## vim-fugitive
在vim中使用 git 命令，常用的 glog、gdiff

## gitv

vim-fugitive 的扩展，更直观的查看 git log，并选择对应 log 进行 diff ，输入 `:gitv` 命令查看效果图:

![gitv](http://upload-images.jianshu.io/upload_images/1952606-18f2267c428cfb2f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

## vim-gitgutter

如果编辑的文件是 git 仓库中的文件，会在左侧标尺处显示当前修改的状态，同时支持在不同修改处进行快速跳转，如下：

![vim-gitgutter](http://upload-images.jianshu.io/upload_images/1952606-7e2af82c17d9f787.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)